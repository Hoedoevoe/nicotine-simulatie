import matplotlib.pyplot as plt

t = 0
secondsInDay = 86400
results = []
while(t < secondsInDay):
    amount = 100
    a = amount * 0.5 ** (t / 7200)
    t = t + 1
    results.append(a)

plt.ylabel('% of amount of nicotine')
plt.xlabel('Amount of seconds')
plt.plot(results)
